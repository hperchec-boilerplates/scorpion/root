# App

[![app](https://img.shields.io/static/v1?label=&message=Scorpion&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion/root)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&labelColor=2496ED&message=Docker&color=2496ED)](https://docker.com/)
[![Discord](https://img.shields.io/discord/972997305739395092?label=Discord&labelColor=5562EA&logo=discord&logoColor=white&color=2F3035)](https://discord.gg/AWTgVAVKaR)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## Requirements

Please refer to the `README.md` file in each submodule repository:
- [Database](https://gitlab.com/hperchec/boilerplates/scorpion/database)
- [Server](https://gitlab.com/hperchec/boilerplates/scorpion/server)
- [UI](https://gitlab.com/hperchec/boilerplates/scorpion/ui)

## Get started

You need to clone all the repositories. I recommend to work with [git submodules](https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules) to easily manage your repositories.

Get inspired by this project structure to reproduce it for yours.

```bash
root/
├── database/    # Submodule: database
├── server/      # Submodule: server
├── ui/          # Submodule: ui
├── ...
```

First, create an empty repository as the *root* of your project and clone https://gitlab.com/hperchec/boilerplates/scorpion/root.git (replace `<your_root_repository>` by your root repository URL):

```bash
# Clone 'root' repo
git clone https://gitlab.com/hperchec/boilerplates/scorpion/root.git
# Move to root folder
cd root
# Remove .git folder
rm -rf .git
# Re-init repo
git init --initial-branch=main
# Replace `<your_root_repository> by your root repository URL)
git remote add origin <your_root_repository>
```

Then, create an **empty repository** for each child repository and clone **inside** the parent folder (`root` by default).

### Database

Make sure you are in root (parent) folder:

```bash
# Clone 'database' child repo
git clone https://gitlab.com/hperchec/boilerplates/scorpion/database.git
# Move to root/database folder
cd database
# Remove .git folder
rm -rf .git
# Re-init repo
git init --initial-branch=main
# Replace `<your_database_repository> by your database repository URL)
git remote add origin <your_database_repository>
# Commit and push
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Server

Make sure you are in root (parent) folder:

```bash
# Clone 'server' child repo
git clone https://gitlab.com/hperchec/boilerplates/scorpion/server.git
# Move to root/server folder
cd server
# Remove .git folder
rm -rf .git
# Re-init repo
git init --initial-branch=main
# Replace `<your_server_repository> by your server repository URL)
git remote add origin <your_server_repository>
# Commit and push
git add .
git commit -m "Initial commit"
git push -u origin main
```

### UI

Make sure you are in root (parent) folder:

```bash
# Clone 'ui' child repo
git clone https://gitlab.com/hperchec/boilerplates/scorpion/ui.git
# Move to root/ui folder
cd ui
# Remove .git folder
rm -rf .git
# Re-init repo
git init --initial-branch=main
# Replace `<your_ui_repository> by your ui repository URL)
git remote add origin <your_ui_repository>
# Commit and push
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Add submodules

Make sure you are in root (parent) folder:

```bash
# Add database submodule
git submodule add <your_database_repository> database
# Add server submodule
git submodule add <your_server_repository> server
# Add ui submodule
git submodule add <your_ui_repository> ui
```

Check your `root/.gitmodules` file:

```bash
[submodule "database"]
	path = database
	url = <your_database_repository>
[submodule "server"]
	path = server
	url = <your_server_repository>
[submodule "ui"]
	path = ui
	url = <your_ui_repository>
```

Finally, commit and push in your 'root' repository:

```bash
# Commit and push
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Configure

Once your structure is okay, please follow "Get started" instructions for each submodule:
- [Database](https://gitlab.com/hperchec/boilerplates/scorpion/database)
- [Server](https://gitlab.com/hperchec/boilerplates/scorpion/server)
- [UI](https://gitlab.com/hperchec/boilerplates/scorpion/ui)

### Enjoy

Now you can start your development 😃

## Directory structure

Let's have a look to the directory structure:

```
root/
├── .docker/                     # Contains docker specific resources
│   ├── .env                     # Defines environment variables used by docker-compose
│   ├── .env.default             # Default .env file
│   └── putty-sessions.reg       # (Windows) PuTTY sessions for SSH connections
├── cmd/                         # Contains useful scripts
│   ├── docker-build.bat         # (Windows) Script to build docker images
│   ├── docker-build.sh          # (Linux) Script to build docker images
│   ├── docker-init.bat          # (Windows) Init script for docker configuration
│   ├── docker-init.sh           # (Linux) Init script for docker configuration
│   ├── docker-up.bat            # (Windows) Script to run docker containers
│   ├── docker-up.sh             # (Linux) Script to run docker containers
│   └── load-putty-sessions.bat  # (Windows) Load PuTTY sessions for each container
├── database/                    # Submodule: database
├── server/                      # Submodule: server
├── ui/                          # Submodule: ui
├── docker-compose.yml           # docker-compose configuration
├── ...
```

## Docker

### Configuration

This project include a built-in command to easily configure **docker-compose**. Just run the following command, depending of your OS (Windows or Linux):

```bash
# Windows environment
./cmd/docker-init
# Linux environment
./cmd/docker-init.sh
```

You can also manually edit the `.docker/.env` file that contains environment variables (expand):

<details>

<summary markdown="span">Environment variables</summary>

|Value                             |Description                                                              |Type      |Default           |
|-                                 |-                                                                        |-         |-                 |
|COMPOSE_PROJECT_NAME              |The name of your project                                                 |*string*  |"App"             |
|DB_IMAGE_NAME                     |The name of the database docker image                                    |*string*  |"mysql-5.7"       |
|DB_CONTAINER_NAME                 |The name of the database container                                       |*string*  |"database"        |
|DB_MYSQL_CONTAINER_PORT           |The port to use for MySQL service in the database container              |*number*  |3306              |
|DB_MYSQL_LOCAL_PORT               |The port to use for MySQL service in the host for the database container |*number*  |3306              |
|DB_SSH_CONTAINER_PORT             |The port to use for SSH service in the database container                |*number*  |22                |
|DB_SSH_LOCAL_PORT                 |The port to use for SSH service in the host for the database container   |*number*  |2201              |
|SERVER_IMAGE_NAME                 |The name of the server docker image                                      |*string*  |"php-7.3.21"      |
|SERVER_CONTAINER_NAME             |The name of the server container                                         |*string*  |"server"          |
|SERVER_LARAVEL_CONTAINER_PORT     |The port to use for Laravel in the server container                      |*number*  |8000              |
|SERVER_LARAVEL_LOCAL_PORT         |The port to use for Laravel in the host for the server container         |*number*  |8000              |
|SERVER_SSH_CONTAINER_PORT         |The port to use for SSH service in the server container                  |*number*  |22                |
|SERVER_SSH_LOCAL_PORT             |The port to use for SSH service in the host for the server container     |*number*  |2202              |
|UI_IMAGE_NAME                     |The name of the UI docker image                                          |*string*  |"node-14.16.1"    |
|UI_CONTAINER_NAME                 |The name of the UI container                                             |*string*  |"ui"              |
|UI_NODE_CONTAINER_PORT            |The port to use in the UI container                                      |*number*  |8080              |
|UI_NODE_LOCAL_PORT                |The port to use in the host for the UI container                         |*number*  |8080              |
|UI_SSH_CONTAINER_PORT             |The port to use for SSH service in the UI container                      |*number*  |22                |
|UI_SSH_LOCAL_PORT                 |The port to use for SSH service in the host for the UI container         |*number*  |2203              |
|NETWORK_NAME                      |The name of the default network created by docker compose                |*string*  |"app-network"     |

</details>

> See also all docker-compose environment variables you can configure in the [documentation](https://docs.docker.com/compose/reference/envvars/).

### Build images

Run this following command:

```bash
# Windows environment
./cmd/docker-build
# Linux environment
./cmd/docker-build.sh
# Or: native command (docker-compose)
docker-compose --env-file .docker/.env build
```

### Run containers

> **WARNING**
>
> Using docker in development for the *ui* submodule in **Windows** environment induces a significant loss of permorance. Please refer to the [ui documentation](https://gitlab.com/hperchec/boilerplates/scorpion/ui).

This project uses docker-compose to run the different necessary containers. Run this following command to run containers:

```bash
# Windows environment
./cmd/docker-up
# Linux environment
./cmd/docker-up.sh
# Or: native command (docker-compose)
docker-compose --env-file .docker/.env up -d
```

### Connect to containers via SSH

> Don't forget that you can use the default docker command to access to the containers: `docker exec -it <name> bash` where `<name>` is the name of the container.
>
> See also [docker exec command reference](https://docs.docker.com/engine/reference/commandline/exec/).

In development, all of these containers can be accessed via SSH connection.

Log in the containers as *root* user **without password**.

#### With PuTTY (Windows)

You can easily import [PuTTY](https://www.putty.org/) sessions if you work in a Windows environment by double-clicking on the `./.docker/putty-sessions.reg` or by running this following command in a **PowerShell** console: `reg import ./.docker/putty-sessions.reg`.

> **WARNING**: Sessions saved in the `.reg` file are based on default SSH port values defined in the `.env.default` file.
>
> You have to edit it **manually** in the PuTTY interface if you want to customize ports!

## Work / Contribute

Clone the main repository with all submodules:

```bash
git clone --recurse-submodules https://gitlab.com/hperchec/boilerplates/scorpion/root.git
```

> See also [git submodules documentation](https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules)

Move to the `root` directory:

```bash
cd root
```

Then, please follow "Get started" instructions for each submodule:
- [Database](https://gitlab.com/hperchec/boilerplates/scorpion/database)
- [Server](https://gitlab.com/hperchec/boilerplates/scorpion/server)
- [UI](https://gitlab.com/hperchec/boilerplates/scorpion/ui)

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)
